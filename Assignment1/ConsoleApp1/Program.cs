﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public void prime()
        {
            bool IsPrime = true;
            Console.WriteLine("Prime numbers :");
            for (int i = 2; i <= 100; i++)
            {
                for (int j = 2; j < 100; j++)
                {
                    if (i != j && i % j == 0)
                    {
                        IsPrime = false;
                        break;
                    }
                }
                if (IsPrime)
                {
                    Console.Write("\t" + i);
                }
                IsPrime = true;
            }
        }
        public void divisible()
        {
            Console.WriteLine("Numbers Divisible by 2, 3 and 5 : ");
            for (int i = 1; i <= 100; i++)
            {
                if ((i % 2 == 0) && (i % 3 == 0) && (i % 5 == 0))
                {
                    Console.WriteLine(i);
                }
            }
        }
        /* Exception Handling */
        public class NameException : Exception
        {
            public NameException(string message) : base(message){}
        }
        /*1.Design grading system based on marks scored.Follow below grade range.
            Marks<40 : Fail.
            40<=Marks<50: 3rd class.
            50<=marks<60: 2nd class.
            60<=marks<66: 1st class.
            66<=marks: distinction.
            Accept name and marks as input.*/
        public void one()
        {
            string name;
            Console.Write("Enter Name : ");
            name = Console.ReadLine();
            Console.Write("Enter Marks : ");
            Int16 marks = Convert.ToInt16(Console.ReadLine());
            if (marks < 40)
            {
                Console.WriteLine("{0} is Failed!", name);
            }
            else if ((marks >= 40) && (marks < 50))
            {
                Console.WriteLine("{0} got 3rd Class!", name);
            }
            else if ((marks >= 50) && (marks < 60))
            {
                Console.WriteLine("{0} got 2nd Class!", name);
            }
            else if ((marks >= 60) && (marks < 66))
            {
                Console.WriteLine("{0} got 1st Class!", name);
            }
            else if (marks >= 66)
            {
                Console.WriteLine("{0} got Distinction!", name);
            }
        }
        /*2.Write a program to find the eligibility of admission for a professional course based on the following criteria: 
            Marks in Maths >=65
            Marks in Phy >=55
            Marks in Chem>=50
            Total in all three subject >=180
            or
            Total in Math and Subjects >=140
            Accept the marks as input.Also display the average(do not round off).*/
        public void two()
        {
            Console.Write("Enter Marks in Maths : ");
            Int16 maths = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter Marks in Physics : ");
            Int16 phy = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter Marks in Chemistry : ");
            Int16 chem = Convert.ToInt16(Console.ReadLine());
            int total = maths + phy + chem;
            float average = (float) total / 3;
            Console.WriteLine("Average Marks : " + average);
            if((maths >= 65) && (phy >= 55) && (chem >= 50) && (total >= 180))
            {
                Console.WriteLine("You are Eligible for Admission!");
            }
            else
            {
                Console.WriteLine("You are not Eligible for Admission!");
            }
        }
        /*3.Write a program to find first 10 multiples of x and y.Find the sum of multiples of x, store it in firstSumOfMultiple.Find the sum of multiples of y, store it in secondSumOfMultiple.
            If firstSumOfMultiple is greater than secondSumOfMultiple then perform
            firstSumOfMultiple /secondSumOfMultiple, else perform secondSumOfMultiple /firstSumOfMultiple.
            Accept x and y as user input, write the output on console.*/
        public void three()
        {
            Console.WriteLine("Enter the value of x : ");
            Int16 x = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Enter the value of y : ");
            Int16 y = Convert.ToInt16(Console.ReadLine());
            int firstSumOfMultiple = 0, secondSumOfMultiple = 0, i;
            for(i=1; i<11; i++)
            {
                firstSumOfMultiple += x * i;
                secondSumOfMultiple += y * i;
            }
            Console.WriteLine("Sum of Multiples of x : " + firstSumOfMultiple);
            Console.WriteLine("Sum of Multiples of y : " + secondSumOfMultiple);
            int res;
            if (firstSumOfMultiple > secondSumOfMultiple)
            {
                res = firstSumOfMultiple / secondSumOfMultiple;
            }
            else
            {
                res = secondSumOfMultiple / firstSumOfMultiple;
            }
            Console.WriteLine("Result : " + res);
        }
        /*4.Write a program to display certain values of the function a = b2 + 2b + 1 
            (using integer numbers for b , ranging from -5 to +5).*/
        public void four()
        {
            Console.WriteLine("Enter the value of b in range -5 to 5 : ");
            Int16 b = Convert.ToInt16(Console.ReadLine());
            if (b<=5 && b>=-5)
            {
                int a = (b * b) + (2 * b) + 1;
                Console.WriteLine("a = " + a);
            }
            else
            {
                Console.WriteLine("You entered invalid value for b!");
            }
        }
        static void Main(string[] args)
        {
            Program p1 = new Program();
            p1.one();
            /*p1.prime();
            Console.WriteLine("\n");
            p1.divisible();
            Console.ReadKey();*/
            Console.ReadKey();
        }
    }
}
