﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    //public delegate double Conversion(double from);   // Assignment 1
    //public delegate int NumberChanger(int n);         // Assignment 2
    public delegate void del1(string s);                // Assignment 3
    public delegate void del2(string s);

    class Program
    {
        /*// ASSIGNMENT 1 
        public static double FeetToInches(double feet)
        {
            return feet * 12;
        }
        static void Main(string[] args)
        {
            Conversion doConversion = new Conversion(FeetToInches);
            Console.Write("Enter Feet : ");
            double feet = Double.Parse(Console.ReadLine());
            double inches = doConversion(feet);
            Console.WriteLine("\n{0} Feet = {1} Inches\n", feet, inches);

            Console.ReadLine();
        }*/

        /*// ASSIGNMENT 2
        public static int AddNum(int a)
        {
            int num = 0;
            num = a + a;
            return num;
        }
        public static int MulNum(int a)
        {
            int num = 0;
            num = a * a;
            return num;
        }
        public static int DivNum(int a)
        {
            int num = 0;
            num = a / a;
            return num;
        }
        static void Main(string[] args)
        {
            Console.Write("Enter Number : ");
            int num = int.Parse(Console.ReadLine());

            NumberChanger nc1 = new NumberChanger(AddNum);       // Add
            int res1 = nc1(num);
            Console.WriteLine("\n{0} + {0} = {1}", num, res1);

            NumberChanger nc2 = new NumberChanger(MulNum);       // Multiply
            int res2 = nc2(num);
            Console.WriteLine("\n{0} * {0} = {1}", num, res2);

            NumberChanger nc3 = new NumberChanger(DivNum);       // Divide
            int res3 = nc3(num);
            Console.WriteLine("\n{0} / {0} = {1}", num, res3);

            Console.ReadLine();
        }*/

        /*// ASSIGNMENT 3
        public static void Good(string s)
        {
            System.Console.WriteLine("\nGood, {0}!", s);
        }
        public static void Morning(string s)
        {
            System.Console.WriteLine("\nMorning, {0}!", s);
        }
        static void Main(string[] args)
        {
            Console.Write("Enter String : ");
            string s = Console.ReadLine();

            del1 d1 = new del1(Good);
            d1(s);
            del2 d2 = new del2(Morning);
            d2(s);

            del1 a, b, c;
            a = Good;
            //a(s);             // Prints output of Good()
            b = Morning;
            c = a + b;
            Console.WriteLine("\nInvoking Delegate for Combined Output : ");
            c(s);

            del1 a1, b1, c1;
            a1 = Good;
            b1 = Morning;
            c1 = c - b1;
            Console.WriteLine("\nInvoking Delegate for Output without del2 Delegate : ");
            c1(s);

            Console.ReadLine();
        }*/

        /*// ASSIGNMENT 4
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Temperature in Fahrenheit : ");
            double celsius;
            double fahrenheit = Convert.ToDouble(Console.ReadLine());
            //double fahrenheit = double.Parse(Console.ReadLine());     // This also works
            celsius = (fahrenheit - 32) * 5 / 9;
            Console.WriteLine("Temperature in Celsius : {0}", celsius);

            Console.ReadLine();
        }*/

        // ASSIGNMENT 5

        static void Main(string[] args)
        {
            string[] digits = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
            string[] d = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
            string[] elevens = { "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            Console.WriteLine("Enter the Number : ");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("Number in Words : ");
            int nextdigit = 0, numdigits = 0, nd = 0;
            int[] n = new int[20];
            int[] m = new int[20];
            do
            {
                nextdigit = num % 10;
                n[numdigits] = nextdigit;
                numdigits++;
                num = num / 10;
            } while (num > 0);
            numdigits--;
            nd--;
            for (; numdigits >= 0; numdigits--)
            {
                if (numdigits == 3)
                {
                    Console.Write("{0} thousand ", digits[n[numdigits]]);
                }
                if (numdigits == 2)
                {
                    Console.Write("{0} hundred ", digits[n[numdigits]]);
                }
                if (numdigits == 1)
                {
                    Console.Write("{0} ", d[n[numdigits + 2]]);
                }
                if (numdigits == 0)
                {
                    Console.Write("{0}", digits[n[numdigits]]);
                }
                //Console.Write(digits[n[numdigits]] + " ");        // To print one two three four
            }
            Console.WriteLine();    // For cursor at next line
            Console.ReadLine();
        }
    }
}
