﻿using System;

namespace Factorial
{
    public class FactorialClass
    {
        public delegate void SendResult(int n);     // delegate
        public event SendResult wheretosend;        // object of delegate ... event when using tweaking [to stop]

        //public void Calculate(int number)
        public void Calculate(int number)
        {
            int result = number;
            for (int i = 1; i < number; i++)
            {
                result = result * i;
            }
            wheretosend(result);
            //return result;
        }
    }
}
