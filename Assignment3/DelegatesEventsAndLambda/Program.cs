﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Factorial;

namespace DelegatesEventsAndLambda
{
    class Program
    {
        static void Main(string[] args)
        {
            FactorialClass fc = new FactorialClass();
            /*int res = fc.Calculate(5);
            Console.WriteLine(res);*/
            //fc.wheretosend = DisplayFactorial;        // "=" in case of single place to send
            fc.wheretosend += DisplayFactorial;         // "+=" in case of multiple places to send
            fc.wheretosend += DisplayFactorialWhenGreaterThan100;
            //fc.wheretosend(10);                         // Tweaking
            fc.Calculate(5);
            //fc.wheretosend -= DisplayFactorial;
            fc.wheretosend += number => { Console.WriteLine("Call Back : " + number); };    // Lambda Function
            fc.wheretosend -= DisplayFactorialWhenGreaterThan100;

            Console.ReadLine();
        }

        static void DisplayFactorial(int number)
        {
            Console.WriteLine("Call Back : " + number);
        }

        static void DisplayFactorialWhenGreaterThan100(int number)
        {
            if (number > 100)
                Console.WriteLine("Factorial Greater than 100");
            else
                Console.WriteLine("Factorial Less than 100");
        }

    }
}
