﻿using MVCDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetStudentsStronglyCoupled()
        {
            List<StudentViewModel> studentList = new List<StudentViewModel>
            {
                new StudentViewModel
                {
                    Id = "1",
                    FirstName = "Anchal",
                    MiddleName = "Ramji",
                    LastName = "Gupta",
                    Age = 21
                },
                new StudentViewModel
                {
                    Id = "2",
                    FirstName = "Palak",
                    MiddleName = "Ram",
                    LastName = "Porwal",
                    Age = 22
                }
            };

            return View(studentList);
        }
        
        public ActionResult CreateStudent()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateStudent(StudentViewModel studentView)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction(nameof(GetStudentsStronglyCoupled));
            }
            return null;
        }
        
        public ActionResult Edit(int Id)
        {
            StudentViewModel student = new StudentViewModel
            {
                Id = "2",
                FirstName = "Palak",
                MiddleName = "Ram",
                LastName = "Porwal",
                Age = 22
            };

            return View(student);
        }
    }
}