﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCDemo.Models
{
    public class StudentViewModel
    {
        [ScaffoldColumn(false)]
        public string Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [StringLength(60)]
        public string MiddleName { get; set; }

        [StringLength(60, MinimumLength = 4)]
        public string LastName { get; set; }

        [MaxLength(24), MinLength(5)]
        public string Title { get; set; }

        [Range(18, 30)]
        public int Age { get; set; }



    }
}