﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class FullTimeEmployee : Person, Interface1
    {
        public void display()
        {
            Console.WriteLine("Name : {0}", Name);
            Console.WriteLine("Address : {0}", Address);
            Console.WriteLine("Age : {0}", Age);
            Console.WriteLine("Employee ID : {0}", EmpId);
            Console.WriteLine("Designation : {0}", Designation);
            Console.WriteLine("Experience : {0}", Experience);
            Console.WriteLine("\n");
        }
        public int EmpId { get; set; }
        public string Designation { get; set; }
        public int Experience { get; set; }
    }
}
