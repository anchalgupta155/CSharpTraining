﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
   public class Program
    {
        static void Main(string[] args)
        {
            FullTimeEmployee p1 = new FullTimeEmployee
            {
                Name = "Anchal",
                Address = "Pune",
                Age = 21,
                EmpId = 52504,
                Designation = "Associate Software Engineer",
                Experience = 1
            };
            p1.display();
            FullTimeEmployee p11 = new FullTimeEmployee
            {
                Name = "Archie",
                Address = "Mumbai",
                Age = 23,
                EmpId = 52540,
                Designation = "Associate Software Engineer",
                Experience = 3
            };
            p11.display();
            if (p1.Designation == p11.Designation)
            {
                Console.WriteLine("Designations are Same! \n");
            }
            else
            {
                Console.WriteLine("Designations are not Same! \n");
            }
            if(p1.Age > p11.Age)
            {
                Console.WriteLine(p1.Name + " is older than " + p11.Name + "\n");
            }
            else
            {
                Console.WriteLine(p1.Name + " is younger than " + p11.Name + "\n");
            }
            Contractor p2 = new Contractor
            {
                Name = "Palak",
                Address = "Mumbai",
                Age = 23,
                EmpId = 52556,
                Designation = "Developer",
                Experience = 3
            };
            p2.display();
            Contractor p22 = new Contractor
            {
                Name = "Lucky",
                Address = "Delhi",
                Age = 26,
                EmpId = 52556,
                Designation = "Senior Developer",
                Experience = 6
            };
            p22.display();
            if (p2.Designation == p22.Designation)
            {
                Console.WriteLine("Designations are Same! \n");
            }
            else
            {
                Console.WriteLine("Designations are not Same! \n");
            }
            if (p2.Age > p22.Age)
            {
                Console.WriteLine(p2.Name + " is older than " + p22.Name + "\n");
            }
            else
            {
                Console.WriteLine(p2.Name + " is younger than " + p22.Name + "\n");
            }
            Class1.pprint(p1);
            Class1.pprint(p11);
            Class1.pprint(p2);
            Class1.pprint(p22);
            Console.ReadKey();
        }
    }
}
