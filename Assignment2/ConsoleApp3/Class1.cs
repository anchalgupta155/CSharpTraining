﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
   public static class Class1
   {
        public static void pprint(this FullTimeEmployee p)
        {
            if (p.Experience < 5)
            {
                Console.WriteLine(p.Name + " has exp <5 yrs ");
            }
            else
            {
                Console.WriteLine(p.Name + " has exp >5 yrs ");
            }
        }

        public static void pprint(this Contractor p)
        {
            if (p.Experience < 5)
            {
                Console.WriteLine(p.Name + " has exp <5 yrs ");
            }
            else
            {
                Console.WriteLine(p.Name + " has exp >5 yrs ");
            }
        }
    }
}
