﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
namespace Assignment7
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stp = new Stopwatch();
            stp.Start();

            List<string> A = new List<string>();
            List<string> B = new List<string>();
            List<string> C = new List<string>();
            List<string> D = new List<string>();

            for(int i=0; i<10000; i++)
            {
                A.Add("A" + i);
            }
            for (int i = 0; i < 10000; i++)
            {
                B.Add("B" + i);
            }
            for (int i = 0; i < 10000; i++)
            {
                C.Add("C" + i);
            }
            for (int i = 0; i < 10000; i++)
            {
                D.Add("D" + i);
            }
            
            stp.Stop();
            Console.WriteLine("Execution Time : " + stp.ElapsedMilliseconds);

            List<string> Final = new List<string>();
            
            for (int i = 0; i < (A.Count + B.Count + C.Count + D.Count); i++)
            {

                int temp = Math.Max((Math.Max(A.Count, B.Count)), Math.Max(C.Count, D.Count));
                if (A.Count == temp)
                {
                    Final.Add(A[0]);
                    A.RemoveAt(0);
                }
                else if (B.Count == temp)
                {
                    Final.Add(B[0]);
                    B.RemoveAt(0);
                }
                if (C.Count == temp)
                {
                    Final.Add(C[0]);
                    C.RemoveAt(0);
                }
                if (D.Count == temp)
                {
                    Final.Add(D[0]);
                    D.RemoveAt(0);
                }

            }

            Final.ForEach(x => Console.Write($"{x} "));
            
            Console.Read();

        }
    }
}
