﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Assignment5
{
    class Program
    {
        public static void emas(string text)
        {
            const string MatchEmailPattern =
           @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";
            Regex rx = new Regex(MatchEmailPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            // Find matches.
            MatchCollection matches = rx.Matches(text);
            // Report the number of matches found.
            int noOfMatches = matches.Count;
            // Report on each match.
            foreach (Match match in matches)
            {
                Console.WriteLine(match.Value.ToString());
            }
        }

        static void Main(string[] args)
        {
            emas("My gmail id is _anc.hal1996@gmail.com.in.in !!!");

            /*// ASSIGNMENT 1
            Dictionary<int, string> d = new Dictionary<int, string>();
            string test;
            d.Add(52504, "Anchal");
            d.Add(52505, "Palak");
            d.Add(52506, "Ashley");
            d.Add(52507, "Barbie");
            d.Add(52508, "Rosie");
            d.Add(52509, "Jill");
            d.Add(52510, "Mary");
            d.Add(52511, "June");
            d.Add(52512, "Dolly");
            d.Add(52513, "Christina");
            for (int i = 0; i < d.Count; i = i + 2){
                Console.WriteLine("Emp. ID : {0} and Emp. Name : {1}", d.Keys.ElementAt(i), d[d.Keys.ElementAt(i)]);
            }*/

            /*// ASSIGNMENT 2
            string str = "My email id is anchal.gupta@xoriant.com\n";
            Console.WriteLine(str);
            int i = str.IndexOf('@');
            string email = str.Substring(i);
            //Console.WriteLine(email);
            int l = str.LastIndexOf(' ');
            string last = str.Substring(l+1);
            Console.WriteLine(last);
            Console.WriteLine(str[0]);

            Regex regex = new Regex(@"\d+");
            Match match = regex.Match("My email id is anchal.gupta@xoriant.com");
            if (match.Success)
            {
                Console.WriteLine(match.Value);
            }*/

            Console.ReadLine();
        }
    }
}
